# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.3.1...v1.4.0) (2021-03-25)


### Features

* add a boilerplate for widget development ([9620251](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/9620251c91cd0c358f3350bb1bd5063b12923fa0))


### Docs

* complete the README with the wordpress-widget boilerplate mention ([1ce67b2](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/1ce67b2a1def0e9eb3de0521ff50d6ed5991379d))

### [1.3.1](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.3.0...v1.3.1) (2021-03-23)


### Bug Fixes

* generate all translatable strings inside watch task ([49a75ad](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/49a75add5b3c7414ecfae1d8ccc5274bd075b0f8))
* use the right function to load textdomain ([5e531cc](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/5e531ccafd7fe4f475d1521890262078a7a6bc17))


### Changed

* add a Gulp function to move fonts from src to assets ([9689880](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/9689880ae952c6ee510eedea1327db5e0e1f3e3c))
* remove babel config from the generated zip ([6b52875](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/6b5287508bee169736aa93b81e0ad5992630f405))

## [1.3.0](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.2.1...v1.3.0) (2021-01-15)


### Features

* add browser option to BrowserSync ([b783663](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/b7836633f2a8f70ba7d3350ec7fbb6a85621e54c))

### [1.2.1](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.2.0...v1.2.1) (2021-01-04)


### Bug Fixes

* enqueue styles and scripts only if they exist ([a5044d5](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/a5044d5b4a2daed765e7706cea79feeb313b37f8))

## [1.2.0](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.1.1...v1.2.0) (2021-01-03)


### Features

* use BrowserSync with local domain instead of localhost ([76653ae](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/76653ae7e2011b9900876c58c2fdd24b358cc92d))

### [1.1.1](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.1.0...v1.1.1) (2020-12-11)


### Bug Fixes

* add onLast option to prevent double notifications ([72302e0](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/72302e09d9ae522f54c3daedcfff422349a64139))

## [1.1.0](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.0.1...v1.1.0) (2020-12-11)


### Features

* add an error handler to prevent interruption of Gulp watch ([a88188a](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/a88188ae95bb92bb0ef13a6f9df3e0e36ddd5a83))

### [1.0.1](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v1.0.0...v1.0.1) (2020-12-10)


### Bug Fixes

* add missing babel declaration ([0747241](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/0747241cd0db16ca7603af81633f5106a1b07d85))

## [1.0.0](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v0.0.2...v1.0.0) (2020-12-09)


### Features

* add and configure Babel to be used in Gulp ([c48e2d8](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/c48e2d80a29e8e593a8232848fc8d060006f7af5))


### Docs

* add instructions for Gulp config, refactor the doc ([80b40fc](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/80b40fc619f3f572fc0110bbcf404686d3c7e45d))
* remove the bug alert concerning bumpCSS ([2310068](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/231006815dd950723ebd09764a1bfe6ad37bc564))

### [0.0.2](https://github.com/ArmandPhilippot/wordpress-boilerplate/compare/v0.0.1...v0.0.2) (2020-12-09)


### Bug Fixes

* bump and recompile CSS with standard-version ([499f0cb](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/499f0cb92604f79bab74828a573b4465d7991232)), closes [#2](https://github.com/ArmandPhilippot/wordpress-boilerplate/issues/2)


### Docs

* fix typo ([c3365ac](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/c3365accbc3f06066b0da8eedaf0fcdf114c3425))

### 0.0.1 (2020-12-07)


### Changed

* initial release ([d4221f4](https://github.com/ArmandPhilippot/wordpress-boilerplate/commit/d4221f4e076acde8505d772b4f362c7a68fabb76))
